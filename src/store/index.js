import Vuex from 'vuex'
import Vue from 'vue'


Vue.use(Vuex)

const state = {
  username: '',
  token: '',
  admin: false,
  showAlert: false,
  alertText: '',
  showSuccess: false,
  successText: ''
}

const getters = {

}

const mutations = {
  
}

const actions = {
  
}

const Store = new Vuex.Store({
  state,
  getters,
  mutations,
  actions
})

export default Store

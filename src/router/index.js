import Vue from 'vue'

import Router from 'vue-router'
import CassandraAnomaly from '../components/CassandraAnomaly'
import HelloWorld from '../components/HelloWorld'

Vue.use(Router) 

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: HelloWorld
    }, 
    {
      path: '/cassandra/anomaly',
      name: 'CassandraAnomaly',
      component: CassandraAnomaly
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})


